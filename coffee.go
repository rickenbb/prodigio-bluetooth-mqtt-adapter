package main

import (
	"encoding/hex"
	"errors"
	"log"
	"os"
	"strings"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/muka/go-bluetooth/api"
	"github.com/muka/go-bluetooth/bluez/profile/adapter"
	"github.com/muka/go-bluetooth/bluez/profile/agent"
	"github.com/muka/go-bluetooth/bluez/profile/device"
)

const (
	authCharUUID   string        = "06aa3a41-f22a-11e3-9daa-0002a5d5c51b"
	brewCharUUID   string        = "06aa3a42-f22a-11e3-9daa-0002a5d5c51b"
	statusCharUUID string        = "06aa3a52-f22a-11e3-9daa-0002a5d5c51b"
	discoveryTime  time.Duration = 2 * time.Second
)

var (
	bluetoothAdapter *adapter.Adapter1
	deviceSecret     []byte
	mac              string
)

func setDeviceSecret() {
	mac = os.Getenv("DEVICE_MAC")

	if os.Getenv("DEVICE_SECRET") == "" {
		log.Fatal("Missing device secret")
	}

	sec, err := hex.DecodeString(os.Getenv("DEVICE_SECRET"))
	if err != nil {
		log.Fatal("Could not parse device secret")
	}
	deviceSecret = sec
}

/*
connectBluetoothAdapter connects to the bluetooth adapter of the raspi.
If anything goes wrong, this method panics as this is run only on startup.
*/
func connectBluetoothAdapter() {
	defer api.Exit()
	connection, err := dbus.SystemBus()
	if err != nil {
		panic(err)
	}

	err = agent.ExposeAgent(connection, agent.NewDefaultSimpleAgent(), agent.CapKeyboardDisplay, true)
	if err != nil {
		panic(err)
	}

	bluetoothAdapter, err = api.GetDefaultAdapter()
	if err != nil {
		panic(err)
	}

	log.Printf("Bluetooth %s adapter connected", bluetoothAdapter.Properties.Address)

	select {}
}

/*
brew takes one input value which determines the volume and triggers
brewing a coffee.
A precondition is that `connectBluetoothAdapter` was run before so that the bluetooth adapter is enabled.
*/
func brew(volume string) error {
	dev, err := findDevice()
	if err != nil {
		log.Printf("Trying to find device, got error: %s", err.Error())
		return err
	}
	defer dev.Disconnect()

	if err := connectDevice(dev); err != nil {
		log.Printf("Trying to connect to device, got error: %s", err.Error())
		return err
	}

	if err := authenticate(dev); err != nil {
		log.Printf("Trying to authenticate, got error: %s", err.Error())
		return err
	}
	log.Printf("Authentication successful")

	time.Sleep(1 * time.Second)

	if err := issueBrewCommand(dev, volume); err != nil {
		log.Printf("Sent brew command, got error: %s", err.Error())
		return err
	}
	return nil
}

/*
findDevice scans for bluetooth devices for a duration defined by the constant `discoveryTime`.
It then filters out the prodigio machine from all the devices in range, returning either
the device we are looking for or an error if it cannot be found.
*/
func findDevice() (*device.Device1, error) {
	if err := bluetoothAdapter.FlushDevices(); err != nil {
		return nil, err
	}

	if err := bluetoothAdapter.StartDiscovery(); err != nil {
		return nil, err
	}

	time.Sleep(discoveryTime)
	if err := bluetoothAdapter.StopDiscovery(); err != nil {
		return nil, err
	}

	devices, err := bluetoothAdapter.GetDevices()
	if err != nil {
		return nil, err
	}

	for _, dev := range devices {
		if !isProdigioMachine(dev.Properties.Alias) {
			continue
		}
		log.Printf("Found device %s with address %s", dev.Properties.Alias, dev.Properties.Address)

		return dev, nil
	}
	return nil, errors.New("Could not find your machine")
}

func connectDevice(dev *device.Device1) error {
	if err := dev.Connect(); err != nil {
		return err
	}

	time.Sleep(1 * time.Second)
	connected, err := dev.GetConnected()
	log.Printf("Connected to machine: %t", connected)
	return err
}

func authenticate(dev *device.Device1) error {
	authChar, err := dev.GetCharByUUID(authCharUUID)
	if err != nil {
		return err
	}

	return authChar.WriteValue(deviceSecret, map[string]interface{}{"type": "request"})
}

/*
issueBrewCommand takes a device and the desired volume (as a string) as input.
Precondition is that `connectDevice` and `authenticate` were successfully run before.
If the input string is not valid (see the `assembleBrewCommand` method) a ristretto
will be brewed.
*/
func issueBrewCommand(dev *device.Device1, volume string) error {
	brewChar, err := dev.GetCharByUUID(brewCharUUID)
	if err != nil {
		return err
	}

	command, err := assembleBrewCommand(volume, "medium")
	if err != nil {
		return err
	}

	return brewChar.WriteValue(command, map[string]interface{}{"type": "request"})
}

/*
isProdigioMachine returns yes when the device name matches the one we are looking for.
If no MAC address is provided in the env variables it looks for any device name starting with "Prodigio_".
If a MAC is provided, it looks for the specific name of that device.
*/
func isProdigioMachine(deviceName string) bool {
	if mac != "" {
		return deviceName == "Prodigio_"+mac
	}
	s := strings.Split(deviceName, "_")
	return s[0] == "Prodigio"
}

func assembleBrewCommand(volume string, temperature string) ([]byte, error) {
	command := "0305070400000000"

	switch temperature {
	case "low":
		command += "01"
	case "medium":
		command += "00"
	case "high":
		command += "02"
	default:
		command += "00"
	}

	switch volume {
	case "ristretto":
		command += "00"
	case "espresso":
		command += "01"
	case "lungo":
		command += "02"
	case "hotwater":
		command += "04"
	case "americano":
		command += "05"
	case "recipe":
		command += "07"
	default:
		command += "01"
	}

	return hex.DecodeString(command)
}
