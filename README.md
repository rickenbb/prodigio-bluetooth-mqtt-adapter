# Prodigio Bridge

This package makes it possible to trigger brewing coffee on a Nespresso Prodigio coffee machine via MQTT. It is intended to be run as a docker container on a bluetooth equipped raspberry pi.

Get the docker image and it's container ID with 
```
sudo docker pull registry.gitlab.com/rickenbb/prodigio-bluetooth-mqtt-adapter
sudo docker image list
```

The command to run the docker container is as follows. Note that we need to allow some degree of access to the host machine from the docker container as we need to access the bluetooth socket.

```
sudo docker run -i -t -d --net=host -v /var/run/dbus:/var/run/dbus --env-file ./.env --privileged <imageid>
```

# Brewing

Publish a message with your preferred cup size (e.g. `espresso`) to the brew topic.
# Config

The following values need to be configured in the `.env` file (start by copying `.env-example`):

| Parameter | Description |
| ------ | ------ |
|`DEVICE_SECRET`| The device secret extracted from your phone (see below) |
|`DEVICE_MAC` (optional)|  This is only needed if there is more than one machine within bluetooth range. The device will have the name 'Prodigio_MAC'. |
|`MQTT_BROKER_URL`|  The url of your MQTT broker. If no broker is provided, the application won't start |
|`MQTT_PORT` (optional)| The port your MQTT broker runs on. Defaults to port 1883 if not explicitly set|
|`MQTT_USERNAME` (optional)|  To be used if the mqtt broker implements access control |
|`MQTT_PASSWORD` (optional)| To be used if the mqtt broker implements access control |
|`MQTT_CLIENT_ID` (optional)| Use this to set the mqtt client id. If not set this defaults to `go_prodigio_client`|
|`BREW_TOPIC` (optional)| The MQQT topic to listen to for brewing. If not set, this defaults to `prodigio/brew`|
|`STATUS_TOPIC` (optional)| The MQQT topic used for status notification. If not set, this defaults to `prodigio/status`|


## Notes

This will only run on Linux. On Mac you will get an error saying the adapter cannot be found.

The device this runs on needs to be within bluetooth connection distance to the coffee machine. Shut down the bluetooth server on your device using `sudo service bluetooth stop` - otherwise it might interfere.

Bluetooth sucks: Sometimes the machine is not found or the brew process fails for no apparent reason. There is a retry mechanism, and if a retry is triggered a notification is published on the status topic.

Important: Turn off bluetooth on your phone or disconnect from the machine, the machine can only connect to one device at a time.

## Getting the bluetooth token

There is a bluetooth logging profile that can be downloaded from apple (developer account needed). See the following links:

https://developer.apple.com/services-account/download?path=/iOS/iOS_Logs/Bluetooth_Logging_Instructions.pdf

https://developer.apple.com/services-account/download?path=/iOS/iOS_Logs/iOSBluetoothLogging.mobileconfig

While the profile is active, pair your phone to the machine. Once done, hit both volume buttons and the lock button briefly. 
Your phone will vibrate to indicate a dump of the logs started.

About 10 minutes later you will see the logs under `Settings/Privacy/Analytics & Improvements/Analytics Data/sysdiagonse_xxxxxx`.

You can then export those (careful, they can be about 300MB) and examine them e.g. using Wireshark. Look for a payload like `8DB84EE4EDFBF461` written to the following characteristic: `06aa3a41-f22a-11e3-9daa-0002a5d5c51b`.

