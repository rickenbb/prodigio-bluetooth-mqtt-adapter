package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
)

const noOfRetries = 5

// These are default values to be overwritten if provided in the .env file
var (
	brewTopic   string = "prodigio/brew"
	statusTopic string = "prodigio/status"
	port        string = "1883"
	clientID    string
)

/*
initializeMQTT connects to the broker with the paramaters provided in the
env file and subscribed to the brew topic.
If anything fails, the method panics to interrupt startup.
*/
func initializeMQTT() {
	if c := os.Getenv("MQTT_CLIENT_ID"); c != "" {
		clientID = c
	} else {
		uid, err := uuid.NewUUID()
		if err != nil {
			panic(err)
		}
		clientID = "prodigio_" + strings.Split(uid.String(), "-")[0]
		fmt.Printf("Connecting with client ID: %s", clientID)
	}

	options := mqtt.ClientOptions{
		ClientID:              clientID,
		Username:              os.Getenv("MQTT_USERNAME"),
		Password:              os.Getenv("MQTT_PASSWORD"),
		DefaultPublishHandler: messagePubHandler,
		OnConnect:             connectHandler,
		OnConnectionLost:      connectLostHandler,
		OnReconnecting:        reconnectHandler,
		AutoReconnect:         true,
		ConnectRetryInterval:  10 * time.Second,
		ConnectRetry:          false,
		ConnectTimeout:        10 * time.Minute,
	}

	if p := os.Getenv("MQTT_PORT"); p != "" {
		port = p
	}

	if brokerURL := os.Getenv("MQTT_BROKER_URL"); brokerURL == "" {
		panic("MQTT: Broker not supplied. Not starting MQQT bridge.")
	} else {
		options.AddBroker(fmt.Sprintf("tcp://%s:%s", brokerURL, port))
	}

	client := mqtt.NewClient(&options)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	sub(client)
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	log.Println("MQTT: Connected")
}

var reconnectHandler mqtt.ReconnectHandler = func(client mqtt.Client, options *mqtt.ClientOptions) {
	log.Println("MQTT: Reconnected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	log.Printf("MQTT: Connect lost: %v", err)
}

func sub(client mqtt.Client) {
	if b := os.Getenv("BREW_TOPIC"); b != "" {
		brewTopic = b
	}

	if s := os.Getenv("STATUS_TOPIC"); s != "" {
		statusTopic = s
	}

	token := client.Subscribe(brewTopic, 2, nil)
	token.Wait()
	log.Printf("MQTT: Subscribed to topic %s \n", brewTopic)
}

/*
messagePubHandler handles all incoming messages but executes a brew action
only when a message for the brew topic is received.
The method tries to brew coffee several times (as specified in `numberOfRetries`) and broadcasts
a message to the status topic upon success, failure or retry.
*/
var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	log.Printf("Received message: %s from topic: %s \n", msg.Payload(), msg.Topic())
	msg.Ack()
	if msg.Topic() != brewTopic {
		return
	}

	var err error
	for i := 0; i < noOfRetries; i++ {
		err = brew(string(msg.Payload()))
		if err != nil {
			client.Publish(statusTopic, 2, true, fmt.Sprintf("Retry %d, error: %s", i+1, err.Error()))
			time.Sleep(500 * time.Millisecond)
			continue
		}
		client.Publish(statusTopic, 2, false, "Enjoy your coffee")
		return
	}
	client.Publish(statusTopic, 2, true, fmt.Sprintf("Could not brew coffee: %s", err.Error()))
}
