package main

import (
	"log"
	"os"
)

func main() {
	log.SetOutput(os.Stdout)
	initializeMQTT()
	setDeviceSecret()
	connectBluetoothAdapter()
}
