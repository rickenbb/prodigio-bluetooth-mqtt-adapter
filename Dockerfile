FROM golang:alpine AS builder

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=arm

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY . .

# Build the application
RUN go build -o main .

FROM scratch
WORKDIR /dist
# RUN apt-get update
# RUN apt-get install -y bluez bluetooth
# Copy our static executable.
COPY --from=builder /build/main .

# Export necessary port
EXPOSE 1883

# Command to run when starting the container
CMD ["/dist/main"]
