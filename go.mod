module gitlab.com/rickenbb/prodigio-bluetooth-mqtt-adapter

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/godbus/dbus/v5 v5.0.4
	github.com/google/uuid v1.1.1
	github.com/muka/go-bluetooth v0.0.0-20210508070623-03c23c62f181
)
